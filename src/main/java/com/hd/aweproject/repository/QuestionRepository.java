package com.hd.aweproject.repository;

import com.hd.aweproject.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Long> {
    Question findById(long id);
    List<Question> findAllByUserId(String userId);
    List<Question> findAll();
    @Transactional
    void deleteByUserId(String userId);
}

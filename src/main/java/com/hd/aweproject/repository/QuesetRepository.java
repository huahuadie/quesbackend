package com.hd.aweproject.repository;

import com.hd.aweproject.domain.Queset;
import com.hd.aweproject.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface QuesetRepository extends JpaRepository<Queset, Long>  {
    List<Queset> findAllByUserId(String userId);
    List<Queset> findAll();
    @Transactional
    void deleteByUserId(String userId);
}

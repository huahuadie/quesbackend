package com.hd.aweproject.service.impl;

import com.hd.aweproject.domain.Queset;
import com.hd.aweproject.domain.Question;
import com.hd.aweproject.repository.QuesetRepository;
import com.hd.aweproject.repository.QuestionRepository;
import com.hd.aweproject.service.QuesetService;
import com.hd.aweproject.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuesetServiceImpl implements QuesetService {
    @Autowired
    private QuesetRepository quesetRepository;
    @Override
    public void addQueset(Queset queset) {quesetRepository.save(queset);}
    @Override
    public List<Queset> searchUserId(String userId) { return quesetRepository.findAllByUserId(userId); }
    @Override
    public List<Queset> searchAll() { return quesetRepository.findAll(); }
    @Override
    public void deleteUserId(String userId) { quesetRepository.deleteByUserId(userId); }
}

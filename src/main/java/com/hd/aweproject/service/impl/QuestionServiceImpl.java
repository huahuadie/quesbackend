package com.hd.aweproject.service.impl;

import com.hd.aweproject.domain.Question;
import com.hd.aweproject.repository.QuestionRepository;
import com.hd.aweproject.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    private QuestionRepository questionRepository;
    @Override
    public void addQuestion(Question question) {
        questionRepository.save(question);
    }
    @Override
    public Question searchId(long id) {
        return questionRepository.findById(id);
    }
    @Override
    public List<Question> searchUserId(String userId) {
        return questionRepository.findAllByUserId(userId);
    }
    @Override
    public List<Question> searchAll() { return questionRepository.findAll(); }
    @Override
    public void deleteUserId(String userId) { questionRepository.deleteByUserId(userId); }
}

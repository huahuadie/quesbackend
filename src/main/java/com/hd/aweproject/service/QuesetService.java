package com.hd.aweproject.service;

import com.hd.aweproject.domain.Queset;

import java.util.List;

public interface QuesetService {
    void addQueset(Queset queset);
    List<Queset> searchUserId(String userId);
    List<Queset> searchAll();
    void deleteUserId(String userId);
}

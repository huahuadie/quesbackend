package com.hd.aweproject.controller;

import com.hd.aweproject.domain.Queset;
import com.hd.aweproject.domain.Question;
import com.hd.aweproject.domain.User;
import com.hd.aweproject.service.QuesetService;
import com.hd.aweproject.service.QuestionService;
import com.hd.aweproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MyController {
    @Autowired
    private QuestionService questionService;
    @GetMapping("/sq")
    public Question searchQuestion(@RequestParam("id") long id) {
        return questionService.searchId(id);
    }
    @GetMapping("/suq")
    public List<Question> searchuQuestion(@RequestParam("userId") String userId) {
        return questionService.searchUserId(userId);
    }
    @GetMapping("/saq")
    public List<Question> searchAllQues() {
        return questionService.searchAll();
    }
    @PostMapping("aq")
    public void addQuestion(@RequestBody Question question) {
        questionService.addQuestion(question);
    }
    @GetMapping("/dq")
    public void deleteQuestionByUserId(@RequestParam("userId") String userId) {
        questionService.deleteUserId(userId);
    }

    @Autowired
    private QuesetService quesetService;
    @GetMapping("/ss")
    public List<Queset> searchuQueset(@RequestParam("userId") String userId) {
        return quesetService.searchUserId(userId);
    }
    @GetMapping("/sas")
    public List<Queset> searchAllSet() {
        return quesetService.searchAll();
    }
    @PostMapping("as")
    public void addQueset(@RequestBody Queset queset) {
        quesetService.addQueset(queset);
    }
    @GetMapping("/ds")
    public void deleteQuesetByUserId(@RequestParam("userId") String userId) {
        quesetService.deleteUserId(userId);
    }

    @Autowired
    private UserService userService;
    @GetMapping("/su")
    public User searchUser(@RequestParam("userId") String userId) {
        return userService.searchUserId(userId);
    }
    @GetMapping("/au")
    public void addUser(@RequestParam("userId") String userId, @RequestParam("password") String password, @RequestParam("userName") String userName, @RequestParam("signature") String signature){
        User user = new User();
        user.setUserId(userId);
        user.setPassword(password);
        user.setUserName(userName);
        user.setSignature(signature);
        userService.addUser(user);
    }
    @GetMapping("/eu")
    public boolean existUser(@RequestParam("userId") String userId){
        return userService.existUserId(userId);
    }
}
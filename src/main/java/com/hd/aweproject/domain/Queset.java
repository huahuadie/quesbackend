package com.hd.aweproject.domain;
import lombok.Data;
import lombok.ToString;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@ToString
@Data
@Component
@Entity
public class Queset {
    @Id
    @GeneratedValue
    private long quesetId;
    private String title;
    private String introduction;
    private Date createdTime;
    private Date modifiedTime;
    private String userId;
}